const QueryWrapper = ({loading, error, ...props}) => {
    if (loading) {
        return <>Chargement...</>
    }

    if (error) {
        console.log(error);
        return <>Erreur :(</>
    }

    return (<>
        {props.children}
    </>);
}

export default QueryWrapper;