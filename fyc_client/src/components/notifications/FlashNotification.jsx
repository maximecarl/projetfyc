const FlashNotification = ({id, content, type, ...props}) => {
    return (
        <div className={`app_notification_flash ${type}`}>
            {content}
        </div>
    )
}

export default FlashNotification;