import {useContext} from "react";
import FlashNotificationsContext from "../../contexts/notifications/FlashNotificationsContext";
import FlashNotification from "./FlashNotification";

const NotificationsContainer = (props) => {
    const {flashNotifications} = useContext(FlashNotificationsContext);

    return (
        <div className={'app_notificationsContainer'}>
            <div className={'app_notificationsContainer-flash'}>
                {flashNotifications.map(notification => <FlashNotification
                    key={`app_flash${notification.id}`}
                    {...notification}
                />)}
            </div>
        </div>
    );
}

export default NotificationsContainer;