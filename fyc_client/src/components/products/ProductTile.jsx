import Tile from "../tiles/Tile";
import {Link} from "react-router-dom";

const ProductTile = ({id, label, price, quantity, seller, categories = []}) => {
    return (
        <Tile title={label}>
            <p>Seller : {seller.username}</p>
            <p>Price : {price} €</p>
            <p>Quantity : {quantity}</p>
            <p>Catégories : {categories.map(category => <span
                key={`product_category_${category.id}`}>
                {category.label}
            </span>)}</p>

            <div className={'app_buttonContainer'}>
                <Link to={`/products/${id}`} className={'app_button app_buttonCta'}>
                    Voir
                </Link>
            </div>
        </Tile>
    );
}

export default ProductTile;