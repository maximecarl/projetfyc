import CommentsProducts from "../comments/CommentsProduct";

const ProductDetail = ({id, label, price, quantity, seller, categories = [], ...props}) => {
    return (
        <div>
            <h1>{label}</h1>

            <p>Prix : {price} €</p>
            <p>Quantité : {quantity}</p>
            { seller && seller.username
                ? <p>Vendeur : {seller.username}</p>
                : null
            }
            <p>Catégories : </p>
            <div style={{marginBottom: '1rem'}}>
                {categories.map((category, index) => 
                    <button className="app_button app_buttonCta" key={index}>{category.label}</button>
                )}
            </div>

            <CommentsProducts productId={id}/>
        </div>
    )
}

export default ProductDetail;