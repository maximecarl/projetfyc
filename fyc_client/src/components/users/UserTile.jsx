import Tile from "../tiles/Tile";

const UserTile = ({id, username, email}) => {
    return (
        <Tile title={username}>
            <p>Données de l'utilisateur</p>
        </Tile>
    );
}

export default UserTile;