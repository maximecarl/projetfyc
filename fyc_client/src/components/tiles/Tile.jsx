const Tile = (props) => {
    const {title, children} = props;

    return (
        <div className={'app_tile app-tile'}>
            <h2 className={'title'}>{title}</h2>
            <div className={'content'}>
                {children}
            </div>
        </div>
    )
}

export default Tile;