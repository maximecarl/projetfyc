const TileList = (props) => {
    return (
      <div className={`app_tileList ${props.className}`}>
          {props.children}
      </div>
    );
}

export const InlineTileList = (props) => {
    return (
        <TileList className={'app_inlineTileList'}>
            {props.children}
        </TileList>
    )
}

export default TileList;