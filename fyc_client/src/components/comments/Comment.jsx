import {useContext} from "react";
import CurrentUserContext from "../../contexts/security/CurrentUserContext";

const Comment = ({content, author = {}, ...props}) => {
    const {currentUser} = useContext(CurrentUserContext);

    return (
        <div className={`app_comment ${currentUser?.id == author.id ? 'app_comment_mine' : ''}`}>
            <h3>{author.username}</h3>
            <p>{content}</p>
        </div>
    );
}

export default Comment;