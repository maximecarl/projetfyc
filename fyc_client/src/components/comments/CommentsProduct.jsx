import {useLazyQuery} from "@apollo/client";
import {COMMENTS_getByProduct} from "../../api/comments/queries";
import {useContext, useEffect, useState} from "react";
import { Link } from "react-router-dom";
import QueryWrapper from "../wrappers/QueryWrapper";
import Comment from "./Comment";
import CreateComment from "./CreateComment";
import CurrentUserContext from "../../contexts/security/CurrentUserContext";

const CommentsProducts = ({productId, ...props}) => {
    const { currentUser } = useContext(CurrentUserContext);
    const [comments, setComments] = useState([]);
    const [getComments, {loading, error}] = useLazyQuery(COMMENTS_getByProduct, {
        variables: {'productId': parseInt(productId)},
        onCompleted: data => {
            if (data.Comments) {
                setComments(data.Comments);
            }
        }
    });

    useEffect(() => {
        getComments();
    }, []);

    return (
        <QueryWrapper loading={loading} error={error}>
            <div className={'app_tile app_tiles_comments'}>
                <h2>Messages</h2>
                { comments.map((comment) => <Comment key={comment.id} {...comment}/>) }
                {
                    currentUser && currentUser.id
                    ? <CreateComment productId={productId}/>
                    : <>
                        <p>Vous devez être connecté pour laisser un commentaire</p>
                        <Link to={'/login'}>
                            <button className="app_button app_buttonCta">Se connecter</button>
                        </Link>
                    </>
                }
            </div>
        </QueryWrapper>
    )
}

export default CommentsProducts;