import {useContext, useState} from "react";
import CurrentUserContext from "../../contexts/security/CurrentUserContext";
import {login} from "../../api/users/security";
import {useMutation} from "@apollo/client";
import {COMMENT_create} from "../../api/comments/mutations";
import QueryWrapper from "../wrappers/QueryWrapper";
import {COMMENT_createInput} from "../../api/comments/input";

const CreateComment = ({productId, ...props}) => {
    const {currentUser} = useContext(CurrentUserContext);
    const [commentData, setCommentData] = useState({
        'content': '',
        'product': productId
    });
    const [COMMENT_createMutation, { loading, error }] = useMutation(COMMENT_create, {
        onCompleted: data => {
            console.log(data);
        }
    });

    const handleChange = (event) => {
        const input = event.target;
        setCommentData({
            ...commentData,
            [input.id]: input.value
        });
    }

    const onSubmit = async (event) => {
        event.preventDefault();

        if (loading) return;
        if (!currentUser) return;

        if (commentData.content && commentData.product) {
            const creation = COMMENT_createMutation({
                variables: {'commentInput': COMMENT_createInput(commentData)}
            });
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <div className={'app_formRow'}>
                <label htmlFor={'content'}></label>
                <div className={'app_formElement_button'}>
                    <textarea
                        type={"text"}
                        id={"content"}
                        name={"content"}
                        value={commentData.content}
                        placeholder={
                            currentUser
                                ? 'Veuillez-vous connecter ou créer un compte pour envoyer un commentaire.'
                                : 'Votre message ici...'
                        }
                        disabled={!currentUser || loading}
                        readOnly={!currentUser || loading}
                        onChange={handleChange}
                    />
                    <input
                        className={'app_button app_buttonCta'}
                        type="submit"
                        value="Envoyer"
                    />
                </div>
            </div>
        </form>
    )
}

export default CreateComment;