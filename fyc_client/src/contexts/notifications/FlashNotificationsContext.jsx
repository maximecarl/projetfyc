import React from "react";

export class FlashNotification {
    constructor(id, content, options) {
        this.id = id;
        this.content = content;
        this.type = options.type ?? 'notice';
        this.duration = options.duration ?? 5000;
    }
}

const FlashNotificationsContext = React.createContext({
    flashNotifications: [],
    setFlashNotification: () => {},
    addFlash: (content, options = {}) => {},
    removeFlash: (id) => {}
});

export default FlashNotificationsContext;