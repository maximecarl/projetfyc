import { gql } from '@apollo/client';

export const COMMENTS_getByProduct = gql`
  query COMMENTS_getByProduct($productId: Int!) {
    Comments(product: $productId) {
      id,
      content,
      author {
        id,
        username
      }
    }
  }
`;