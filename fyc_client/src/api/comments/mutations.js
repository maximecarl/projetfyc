import { gql } from '@apollo/client';

export const COMMENT_create = gql`
  mutation COMMENT_create($commentInput: CreateCommentInput!) {
    CreateComment(input: $commentInput) {
      id
    }
  }
`;