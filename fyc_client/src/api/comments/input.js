export const COMMENT_createInput = (commentData) => {
    return {
        content: commentData.content ?? '',
        product: parseInt(commentData.product)
    }
}