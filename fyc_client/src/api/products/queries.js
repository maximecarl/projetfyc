import { gql } from '@apollo/client';

export const PRODUCT_getById = gql`
  query PRODUCT_getById($userId: Int!) {
    Product(id: $userId) {
      id
      label
      description
      price
      quantity
      seller {
        id
        username
      }
      categories {
        id
        label
      }
    }
  }
`;

export const PRODUCTS_availableList = gql`
  query PRODUCTS_availableList($hideMine: Boolean) {
    Products(availability: true, hideMine: $hideMine) {
      id
      label
      description
      price
      quantity
      seller {
        id
        username
      }
      categories {
        id
        label
      }
    }
  }
`;