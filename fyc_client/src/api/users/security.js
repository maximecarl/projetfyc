const LOCAL_STORAGE_TOKEN = 'token';

/**
 * Returns a boolean if token is found
 * Data : {username: String, password: String}
 *
 * @param data
 * @returns {Promise<null|boolean>}
 */
export const login = async (data) => {
    if (!data.username || !data.password) {
        return null;
    }

    const options = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    };

    const token = await fetch(`${process.env.REACT_APP_API_URL}login`, options)
        .then(response => response.json())
        .then(response => response.token);

    // Save the token in localStorage
    setToken(token);

    // Return token found
    return token !== null && token !== undefined;
}

/**
 * Set or change the token stored in localStorage
 * @param token
 */
export const setToken = token => {
    localStorage.setItem(LOCAL_STORAGE_TOKEN, token);
}

/**
 * Returns a token if the user is already logged
 *
 * @returns {string}
 */
export const getToken = () => {
    // Return the token in localStorage
    return localStorage.getItem(LOCAL_STORAGE_TOKEN);
}

/**
 * Remove the token stored in localStorage
 */
export const removeToken = () => {
    return localStorage.removeItem(LOCAL_STORAGE_TOKEN);
}