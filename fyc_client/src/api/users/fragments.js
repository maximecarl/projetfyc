import {gql} from "@apollo/client";

export const USER_profileFragment = gql`
  fragment USER_profileFragment on User {
    id
    username
    email
  }
`;