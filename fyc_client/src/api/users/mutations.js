import { gql } from '@apollo/client';
import {USER_profileFragment} from "./fragments";

export const USER_update = gql`
  ${USER_profileFragment}
  
  mutation USER_update($userInput: UpdateUserInput!) {
    UpdateUser(input: $userInput) {
      ...USER_profileFragment
    }
  }
`;