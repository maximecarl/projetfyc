import { gql } from '@apollo/client';

export const USER_getProfile = gql`
  query USER_getProfile {
    UserProfile {
      id,
      username,
      email
    }
  }
`;

export const USERS_findAll = gql`
  query USERS_findAll {
    Users {
      id,
      username,
      email
    }
  }
`;