import React, {useState, useCallback } from "react";
import CurrentUserContext from "../contexts/security/CurrentUserContext";
import FlashNotificationsContext, {FlashNotification} from "../contexts/notifications/FlashNotificationsContext";

let idFlash = 0;

const AppProvider = (props) => {
    const [currentUser, setCurrentUser] = useState(null);

    const [flashNotifications, setFlashNotifications] = useState([]);
    const addFlash = (content, options = {}) => {
        const id = idFlash ++;
        const notification = new FlashNotification(id, content, options);
        setFlashNotifications((flashNotifications) => {
            return [...flashNotifications, notification]
        });

        if (notification.duration !== Infinity) {
            setTimeout(() => {
                removeFlash(id);
            }, notification.duration);
        }
    }
    const removeFlash = (id) => {
        setFlashNotifications((flashNotifications) => {
            return flashNotifications.filter(notification => notification.id !== id)
        });
    }

    return (
        <CurrentUserContext.Provider value={{currentUser, setCurrentUser}}>
            <FlashNotificationsContext.Provider value={{flashNotifications, setFlashNotifications, addFlash, removeFlash}}>
                {props.children}
            </FlashNotificationsContext.Provider>
        </CurrentUserContext.Provider>
    )
}

export default AppProvider;