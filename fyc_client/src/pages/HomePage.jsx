const HomePage = (props) => {
    return (
        <>
            <h1>Bienvenue !</h1>
            <p>Durant ce TP, nous aborderons différents éléments utiles à GraphQL pour créer une API avec Symfony.</p>
            <p>Les étapes à suivre seront les suivantes :</p>
            <p>En suivant le live coding, vous apprendrez à : (branche feature/start)</p>
            <ul>
               <li>Créer le type Product</li>
               <li>Créer la Query Products</li>
               <li>Créer la Mutation CreateProduct</li>
            </ul>
            <p>En autonomie, il vous sera demandé de : (branche feature/autonomie)</p>
            <ul>
               <li>Créer le type Comment</li>
               <li>Créer la Query Comments</li>
               <li>Créer la Mutation CreateComment</li>
            </ul>
            <p>Bon courage</p>
        </>
    );
}

export default HomePage;