const LoadingPage = (props) => {
    return (
        <main className={'app_loadingPage'}>
            Chargement de l'application...
        </main>
    );
}

export default LoadingPage;