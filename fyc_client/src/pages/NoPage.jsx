const NoPage = (props) => {
    return (
        <div>
            <h1>Page non trouvée</h1>
        </div>
    );
}

export default NoPage;