import {useContext, useState} from "react";
import CurrentUserContext from "../../contexts/security/CurrentUserContext";
import {removeToken} from "../../api/users/security";
import {useMutation} from "@apollo/client";
import {USER_update} from "../../api/users/mutations";
import FlashNotificationsContext from "../../contexts/notifications/FlashNotificationsContext";

const UserProfile = (props) => {
    const {currentUser, setCurrentUser} = useContext(CurrentUserContext);
    const [isEditing, setIsEditing] = useState(false);
    const [userData, setUserData] = useState({
        'username': currentUser.username,
        'email': currentUser.email
    });
    const {addFlash} = useContext(FlashNotificationsContext);

    const [USER_updateMutation, {loading, error}] = useMutation(USER_update, {
        onCompleted : data => {
            if (data.UpdateUser) {
                addFlash('Votre profil vient d\'être modifié.\nVeuillez-vous reconnecter.', {
                    type: 'success'
                });
                removeToken();
                setCurrentUser(null);
            } else {
                setIsEditing(false);
            }
        }
    });

    const toggleEditing = () => {
        setIsEditing(!isEditing);
    }

    const handleChange = (event) => {
        const input = event.target;
        setUserData({
            ...userData,
            [input.id]: input.value
        });
    }

    const handleLogout = () => {
        setCurrentUser(null);
        removeToken();
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        const userInput = {};
        if (userData.username && userData.username !== currentUser.username) {
            userInput.username = userData.username;
        }

        if (userData.email && userData.email !== currentUser.email) {
            userInput.email = userData.email;
        }

        if (Object.keys(userInput).length) {
            const update = USER_updateMutation({
                variables: {'userInput': userInput}
            });
        } else {
            toggleEditing();
        }
    }

    return (
        <>
            <h1>Profil</h1>

            <form onSubmit={handleSubmit}>
                {
                    isEditing
                        ? <div className={'app_formRow'}>
                            <label htmlFor={'username'}>Nom d'utilisateur</label>
                            <input
                                id='username'
                                name='username'
                                type='text'
                                value={userData.username}
                                onChange={handleChange}
                            />
                        </div>
                        : <div className={'app_formTextRow'}>
                            <p>Nom d'utilisateur</p>
                            <p className={'app_formText'}>{currentUser.username}</p>
                        </div>
                }



                {
                    isEditing
                        ? <div className={'app_formRow'}>
                            <label htmlFor={'username'}>Email</label>
                            <input
                                id='email'
                                name='email'
                                type='text'
                                value={userData.email}
                                onChange={handleChange}
                            />
                        </div>
                        : <div className={'app_formTextRow'}>
                            <p>Email</p>
                            <p className={'app_formText'}>{currentUser.email}</p>
                        </div>
                }

                <div className={'app_buttonContainer'}>
                    <button type='button' className={'app_button app_buttonDanger'} style={{marginRight: 'auto'}} onClick={handleLogout}>
                        Déconnexion
                    </button>
                    <button type='button' className={`app_button ${isEditing ? 'app_buttonDanger': 'app_buttonCta'}`} onClick={toggleEditing}>
                        {isEditing ? 'Annuler': 'Modifier'}
                    </button>
                    {
                        isEditing
                            ? <button type='submit' className={'app_button app_buttonCta'}>
                                Envoyer
                            </button>
                            : ''
                    }
                </div>
            </form>
        </>
    );
}

export default UserProfile;