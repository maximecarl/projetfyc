import {useQuery} from "@apollo/client";
import {USERS_findAll} from "../../api/users/queries";
import UserTile from "../../components/users/UserTile";
import QueryWrapper from "../../components/wrappers/QueryWrapper";

const UsersPage = (props) => {
    const { usersLoading, usersError, usersData } = useQuery(USERS_findAll);

    if (usersLoading) {
        return <>Chargement...</>
    }

    return (
      <QueryWrapper loading={usersLoading} error={usersError}>
          <h1>Utilisateurs</h1>
          <div>
              {
                  usersData
                      ? usersData.map((user) => <UserTile {...user}/>)
                      : <p>Aucun résultat</p>
              }
          </div>
      </QueryWrapper>
    );
}

export default UsersPage;