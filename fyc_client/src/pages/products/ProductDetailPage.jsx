import {useEffect, useState} from "react";
import {useLazyQuery, useQuery} from "@apollo/client";import {PRODUCT_getById} from "../../api/products/queries";
import {useParams} from "react-router-dom";
import QueryWrapper from "../../components/wrappers/QueryWrapper";
import ProductDetail from "../../components/products/ProductDetail";

const ProductDetailPage = (props) => {
    const { id } = useParams();

    const [product, setProduct] = useState(null);
    const [getProduct, { loading, error }] = useLazyQuery(PRODUCT_getById, {
        variables: {'userId': parseInt(id)},
        onCompleted: data => {
            if (data.Product) {
                setProduct(data.Product);
            }
        }
    });

    useEffect(() => {
        getProduct();
    }, []);

    return (
      <>
          <QueryWrapper loading={loading} error={error}>
              <ProductDetail {...product}/>
          </QueryWrapper>
      </>
    );
}

export default ProductDetailPage;