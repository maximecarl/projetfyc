import {useQuery} from "@apollo/client";
import {PRODUCTS_availableList} from "../../api/products/queries";
import QueryWrapper from "../../components/wrappers/QueryWrapper";
import {useContext, useState} from "react";
import ProductTile from "../../components/products/ProductTile";
import TileList, {InlineTileList} from "../../components/tiles/TileList";
import CurrentUserContext from "../../contexts/security/CurrentUserContext";

const ProductsListPage = (props) => {
    const {currentUser} = useContext(CurrentUserContext);
    const [products, setProducts] = useState([]);

    const { loading, error } = useQuery(PRODUCTS_availableList, {
        onCompleted: (data) => {
            if (data.Products) {
                setProducts(data.Products);
            }
        }
    });

    return (
        <>
            <h1>Produits</h1>
            {
                currentUser
                    ? <>
                        <h2>Mes produits en vente</h2>
                        <QueryWrapper loading={loading} error={error}>
                            <InlineTileList>
                                {
                                    products.length
                                        ? products
                                            .filter(product => product?.seller?.username === currentUser?.username)
                                            .map(product => <ProductTile
                                                key={`product_${product.id}`}
                                                {...product}
                                            />)
                                        : 'Aucun produit trouvé'
                                }
                            </InlineTileList>
                        </QueryWrapper>
                    </>
                    : ''
            }
            <h2>Découvrir des produits</h2>
            <QueryWrapper loading={loading} error={error}>
                <TileList>
                    {
                        products.length
                            ? products
                                .filter(product => product?.seller?.username !== currentUser?.username )
                                .map(product => <ProductTile
                                    key={`product_${product.id}`}
                                    {...product}
                                />)
                            : 'Aucun produit trouvé'
                    }
                </TileList>
            </QueryWrapper>
        </>
    );
}

export default ProductsListPage;