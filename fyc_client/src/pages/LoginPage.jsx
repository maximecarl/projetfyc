import {useContext, useEffect, useState} from "react";
import CurrentUserContext from "../contexts/security/CurrentUserContext";
import {getToken, login} from "../api/users/security";
import {useLazyQuery} from "@apollo/client";
import {USER_getProfile} from "../api/users/queries";
import Tile from "../components/tiles/Tile";

const LoginPage = (props) => {
    const {currentUser, setCurrentUser} = useContext(CurrentUserContext);
    const [userData, setUserData] = useState({
        'username': '',
        'password': ''
    });
    const [getCurrentUser, { loadingCurrentUser }] = useLazyQuery(USER_getProfile, {
        onCompleted: data => {
            if (data.UserProfile) {
                setCurrentUser({
                    ...currentUser,
                    ...data.UserProfile
                })
            }
        }
    });

    const handleChange = (event) => {
        const input = event.target;
        setUserData({
            ...userData,
            [input.id]: input.value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Set JWT Token
        if (userData.username && userData.password) {
            if (await login(userData)) {
                // Get connected user
                getCurrentUser();
            }
        }
    }

    return (
        <div>
            <h1>Connexion</h1>

            {
                loadingCurrentUser
                    ? <p>Chargement...</p>
                    : <form
                        onSubmit={handleSubmit}
                    >
                        <div className={'app_formRow'}>
                            <label htmlFor={"username"}>
                                Username
                            </label>
                            <input
                                id="username"
                                name="username"
                                value={userData.username}
                                type="text"
                                onChange={handleChange}
                            />
                        </div>
                        <div className={'app_formRow'}>
                            <label htmlFor={"password"}>
                                Password
                            </label>
                            <input
                                id="password"
                                name="password"
                                value={userData.password}
                                type="password"
                                onChange={handleChange}
                            />
                        </div>
                        <div className={'app_buttonContainer'}>
                            <input
                                className={'app_button app_buttonCta'}
                                type="submit"
                                value="Me connecter"
                            />
                        </div>
                    </form>
            }
        </div>
    );
}

export default LoginPage;