import { Outlet, Link } from "react-router-dom";
import {useContext} from "react";
import CurrentUserContext from "../contexts/security/CurrentUserContext";
import NotificationsContainer from "../components/notifications/NotificationsContainer";

const Layout = (props) => {
    const {currentUser} = useContext(CurrentUserContext);

    return (
        <>
            <header className={'app_menu'}>
                <nav>
                    <ul>
                        <li>
                            <Link to={'/'}>Accueil</Link>
                        </li>
                        <li>
                            <Link to={'/products'}>Produits</Link>
                        </li>
                        <li>
                            <Link to={'/users/profile'}>{currentUser ? 'Profil' : 'Connexion'}</Link>
                        </li>
                    </ul>
                </nav>
            </header>

            <NotificationsContainer/>

            <main>
                <Outlet />
            </main>
        </>
    );
}

export default Layout;