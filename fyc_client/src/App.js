import './assets/styles/index.scss';
import React, {useContext, useEffect, useState} from "react";
import CurrentUserContext from "./contexts/security/CurrentUserContext";
import {useLazyQuery} from "@apollo/client";
import {USER_getProfile} from "./api/users/queries";
import {getToken, removeToken} from "./api/users/security";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";

import {default as ProductsLayout} from "./pages/products/Layout";
import ProductsListPage from "./pages/products/ProductsListPage";
import ProductDetailPage from "./pages/products/ProductDetailPage";

import {default as DiscussionsLayout} from "./pages/discussions/Layout";
import DiscussionsListPage from "./pages/discussions/DiscussionsListPage";
import DiscussionDetailPage from "./pages/discussions/DiscussionDetailPage";

import {default as UsersLayout} from "./pages/users/Layout";
import UserProfile from "./pages/users/UserProfile";

import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import NoPage from "./pages/NoPage";
import AppProvider from "./providers/AppProvider";
import LoadingPage from "./pages/LoadingPage";

function App() {
    return (
      <AppProvider>
        <BrowserRouter>
            <AppRoutes/>
        </BrowserRouter>
      </AppProvider>
  );
}

const AppRoutes = (props) => {
    const [appLoading, setAppLoading] = useState(true);
    const {currentUser, setCurrentUser} = useContext(CurrentUserContext);

    const [getCurrentUser, { loadingCurrentUser, errorCurrentUser }] = useLazyQuery(USER_getProfile, {
        onCompleted: data => {
            if (data.UserProfile) {
                setCurrentUser({
                    ...currentUser,
                    ...data.UserProfile
                })
            } else {
                removeToken();
                setCurrentUser(null);
            }
            setAppLoading(false);
        },
        onError: error => {
            removeToken();
            setCurrentUser(null);
            setAppLoading(false);
        }
    });

    useEffect(() => {
        const token = getToken();
        if (token) {
            // Get connected user
            const getProfile = getCurrentUser();
        } else {
            setAppLoading(false);
        }
    }, []);

    if (appLoading) {
        return (
            <Routes>
                <Route path="*" element={<LoadingPage />} />
            </Routes>
        );
    }

    return (
        <Routes>
            <Route path="/" element={<Layout />}>

                <Route path="/products/" element={<ProductsLayout />}>
                    <Route index element={<ProductsListPage />} />
                    <Route path=":id" element={<ProductDetailPage />} />
                </Route>

                {
                    currentUser
                        ? <Route path="/discussions/" element={<DiscussionsLayout/>}>
                            <Route index element={<DiscussionsListPage/>}/>
                            <Route path=":id" element={<DiscussionDetailPage/>}/>
                        </Route>
                        : ''
                }

                {
                    currentUser
                        ? <Route path="/users/" element={<UsersLayout />}>
                            <Route path="profile" element={<UserProfile />} />
                        </Route>
                        : ''
                }

                <Route index element={<HomePage />} />
                {
                    currentUser
                        ? <Route path="*" element={<NoPage />} />
                        : <Route path="*" element={<LoginPage />} />
                }
            </Route>
        </Routes>
    );
}

export default App;
