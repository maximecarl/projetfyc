<?php

namespace App\Service;

use Doctrine\ORM\Query;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class QueryHelper
{
    private $queryBuilder;
    private string $entityName;

    /**
     * QueryHelper constructor.
     * @param ServiceEntityRepository $entityRepository
     * @param string $entityName
     */
    public function __construct(
        ServiceEntityRepository $entityRepository,
        string $entityName
    ) {
        $this->queryBuilder = $entityRepository->createQueryBuilder($entityName);
        $this->entityName = $entityName;
    }

    /**
     * @param string $fieldName
     * @param $value
     * @return $this
     */
    public function addEqualValue(string $fieldName, $value): self
    {
        if ($value !== null) {
            $this->queryBuilder
                ->andWhere($this->entityName.'.'.$fieldName.' = :'.$fieldName)
                ->setParameter($fieldName, $value);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param $value
     * @return $this
     */
    public function addNotEqualValue(string $fieldName, $value): self
    {
        if ($value !== null) {
            $this->queryBuilder
                ->andWhere($this->entityName.'.'.$fieldName.' != :'.$fieldName)
                ->setParameter($fieldName, $value);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param $value
     * @return $this
     */
    public function addMinValue(string $fieldName, $value): self
    {
        if ($value !== null) {
            $this->queryBuilder
                ->andWhere($this->entityName.'.'.$fieldName.' >= :'.$fieldName)
                ->setParameter($fieldName, $value);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param $value
     * @return $this
     */
    public function addMaxValue(string $fieldName, $value): self
    {
        if ($value !== null) {
            $this->queryBuilder
                ->andWhere($this->entityName.'.'.$fieldName.' <= :'.$fieldName)
                ->setParameter($fieldName, $value);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param $value
     * @return $this
     */
    public function addEqualString(string $fieldName, $value): self
    {

        if ($value !== null) {
            $this->queryBuilder
                ->andWhere("UPPER($this->entityName.$fieldName) = UPPER(:$fieldName)")
                ->setParameter($fieldName, $value);
        }
        return $this;
    }

    /**
     * @param array $fieldNames
     * @param string|null $valueLike
     * @return $this
     */
    public function addLikeString(array $fieldNames, ?string $valueLike): self
    {
        if ($valueLike !== null && !empty($fieldNames)) {
            $valueLike = strtolower($valueLike);
            $orStatements = $this->queryBuilder->expr()->orX();
            while (!empty($fieldNames)) {
                $fieldName = array_shift($fieldNames);
                $orStatements->add(
                    $this->queryBuilder->expr()->like(
                        $this->queryBuilder->expr()->lower($this->entityName .'.'.$fieldName),
                        $this->queryBuilder->expr()->literal('%' . $valueLike . '%')
                    )
                );
            }
            $this->queryBuilder->andWhere($orStatements);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param array $values
     * @return $this
     */
    public function addValuesIn(string $fieldName, array $values): self
    {
        if ($values && is_array($values) && count($values)) {
            $this->queryBuilder
                ->andWhere($this->entityName.'.'.$fieldName.' IN (:'.$fieldName.')')
                ->setParameter($fieldName, array_values($values));
        }

        return $this;
    }

    /**
     * @param string $fieldName
     * @param int|null $memberValue
     * @return $this
     */
    public function addValueMemberOf(string $fieldName, ?int $memberValue): self
    {
        if ($memberValue !== null) {
            $this->queryBuilder->andwhere(':value MEMBER OF '.$this->entityName.'.'.$fieldName)
                ->setParameter('value', $memberValue);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param string|null $order
     * @return $this
     */
    public function addOrderBy(string $fieldName, ?string $order): self
    {
        if ($order !== null) {
            $this->queryBuilder->orderBy($this->entityName.'.'.$fieldName, $order);
        }
        return $this;
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        return $this->queryBuilder->getQuery();
    }

    /**
     * @return int|mixed|string
     */
    public function execute()
    {
        return $this->getQuery()
            ->getResult();
    }
}