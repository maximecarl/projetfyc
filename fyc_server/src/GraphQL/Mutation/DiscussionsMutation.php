<?php
# src/GraphQL/Mutation/DiscussionsMutation.php
namespace App\GraphQL\Mutation;

use App\Entity\Discussion;
use App\Entity\Message;
use App\Entity\Product;
use App\GraphQL\Input\DeleteObjectInput;
use App\GraphQL\Input\Discussion\CreateDiscussionInput;
use App\GraphQL\Input\Discussion\SendMessageDiscussionInput;
use App\GraphQL\Input\Discussion\UpdateMessageDiscussionInput;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DiscussionsMutation extends AbstractMutation
{
    /**
     * DiscussionsMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__constructor('Discussion', $entityManager, $validator, $tokenStorage);
    }

    /**
     * @param Argument $args
     * @return Discussion|null
     */
    public function create(Argument $args): Discussion
    {
        $inputData = $args->offsetGet('input');
        $discussionInput = new CreateDiscussionInput($inputData);

        $errors = $this->validator->validate($discussionInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            ->find($discussionInput->product)
        ;

        if (!$product || !$product instanceof Product) {
            return $this->createNotFoundException($discussionInput->product, 'Product');
        }

        $seller = $product->getSeller();
        $buyer = $this->getUser();

        if ($seller->getId() === $buyer->getId()) {
            return $this->createUnauthorizedActionException('[Discussion] - You cannot discussion about your own product.');
        }

        $discussion = new Discussion();
        $discussion->setProduct($product);
        $discussion->addUser($buyer);
        $discussion->addUser($seller);

        $this->entityManager->persist($discussion);
        $this->entityManager->flush();

        return $discussion;
    }

    /**
     * @param Argument $args
     * @return Message
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function sendMessage(Argument $args): Message
    {
        $inputData = $args->offsetGet('input');
        $messageInput = new SendMessageDiscussionInput($inputData);

        $errors = $this->validator->validate($messageInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $discussion = $this->entityManager
            ->getRepository(Discussion::class)
            ->find($messageInput->discussion)
        ;

        if (!$discussion || !$discussion instanceof Discussion) {
            return $this->createNotFoundException($messageInput->discussion);
        }

        $currentUser = $this->getUser();
        if (!in_array($currentUser, $discussion->getUsers()->toArray())) {
            return $this->createUnauthorizedActionException('[Discussion] - You cannot send a message in this discussion.');
        }

        $message = new Message();
        $message->setAuthor($currentUser);
        $message->setContent($messageInput->content);

        $this->entityManager->persist($message);
        $discussion->addMessage($message);
        $this->entityManager->flush();

        return $message;
    }

    /**
     * @param Argument $args
     * @return Message
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function updateMessage(Argument $args): Message
    {
        $inputData = $args->offsetGet('input');
        $messageInput = new UpdateMessageDiscussionInput($inputData);

        $errors = $this->validator->validate($messageInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $message = $this->entityManager
            ->getRepository(Message::class)
            ->find($messageInput->id)
        ;

        if (!$message || !$message instanceof Message) {
            return $this->createNotFoundException($messageInput->id);
        }

        $currentUser = $this->getUser();
        if (!$message->getAuthor() === $currentUser) {
            return $this->createUnauthorizedActionException('[Discussion] - You cannot edit a message from an other person.');
        }

        $message->setContent($messageInput->content);
        $this->entityManager->flush();

        return $message;
    }

    /**
     * @param Argument $args
     * @return bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function leave(Argument $args): bool
    {
        $inputData = $args->offsetGet('input');
        $discussionInput = new DeleteObjectInput($inputData);

        $errors = $this->validator->validate($discussionInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $discussion = $this->entityManager
            ->getRepository(Discussion::class)
            ->find($discussionInput->id)
        ;

        if (!$discussion || !$discussion instanceof Discussion) {
            return $this->createNotFoundException($discussionInput->id);
        }

        $currentUser = $this->getUser();
        if (!in_array($currentUser, $discussion->getUsers()->toArray())) {
            return $this->createUnauthorizedActionException('[Discussion] - You cannot leave this discussion.');
        }

        if (count($discussion->getUsers()) === 1) {
            $this->entityManager->remove($discussion);
        } else {
            $discussion->removeUser($currentUser);
        }
        $this->entityManager->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'Discussion_create',
            'sendMessage' => 'Discussion_sendMessage',
            'updateMessage' => 'Discussion_updateMessage',
            'leave' => 'Discussion_leave',
        ];
    }
}