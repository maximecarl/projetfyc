<?php
# src/GraphQL/Mutation/UserMutation.php
namespace App\GraphQL\Mutation;

use App\Entity\User;
use App\GraphQL\Input\User\CreateUserInput;
use App\GraphQL\Input\DeleteObjectInput;
use App\GraphQL\Input\User\UpdateUserInput;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UsersMutation extends AbstractMutation
{
    private UserPasswordHasherInterface $passwordHasher;

    /**
     * UsersMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $passwordHasher
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        UserPasswordHasherInterface $passwordHasher,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__constructor('User', $entityManager, $validator, $tokenStorage);
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @param Argument $args
     * @return User
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function create(Argument $args): User
    {
        $inputData = $args->offsetGet('input');
        $userInput = new CreateUserInput($inputData);

        $errors = $this->validator->validate($userInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        /** @var User $user */
        $user = $this->fillObjectFromInput(new User(), $userInput);
        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            $user->getPassword()
        ));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param Argument $args
     * @return User
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function update(Argument $args): User
    {
        $inputData = $args->offsetGet('input');
        $userInput = new UpdateUserInput($inputData);

        $errors = $this->validator->validate($userInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $user = $this->getUser();

        if (!$user || !$user instanceof User) {
            return $this->createNotFoundException($userInput->id);
        }

        $user = $this->fillObjectFromInput($user, $userInput);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param Argument $args
     * @return bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete(Argument $args): bool
    {
        $inputData = $args->offsetGet('input');
        $userInput = new DeleteObjectInput($inputData);

        $errors = $this->validator->validate($userInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $user = $this->getUser();

        if (!$user || !$user instanceof User) {
            return $this->createNotFoundException($userInput->id);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'User_create',
            'update' => 'User_update',
            'delete' => 'User_delete',
        ];
    }
}