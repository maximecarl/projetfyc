<?php
# src/GraphQL/Mutation/ProductsMutation.php
namespace App\GraphQL\Mutation;

use App\Entity\Category;
use App\Entity\Product;
use App\GraphQL\Input\Category\AddOrRemoveCategoriesToProductInput;
use App\GraphQL\Input\Product\CreateProductInput;
use App\GraphQL\Input\DeleteObjectInput;
use App\GraphQL\Input\Product\UpdateProductInput;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductsMutation extends AbstractMutation
{
    /**
     * UsersMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__constructor('Product', $entityManager, $validator, $tokenStorage);
    }

    /**
     * @param Argument $args
     * @return Product
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function create(Argument $args): Product
    {
        $inputData = $args->offsetGet('input');
        $productInput = new CreateProductInput($inputData);

        $errors = $this->validator->validate($productInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        /** @var Product $product */
        $product = $this->fillObjectFromInput(new Product(), $productInput);
        $product->setSeller($this->getUser());

        $categories = $this->entityManager
            ->getRepository(Category::class)
            ->findBy([
                'id' => $productInput->categories
            ])
        ;

        foreach ($categories as $category) {
            $product->addCategory($category);
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param Argument $args
     * @return Product
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function update(Argument $args): Product
    {
        $inputData = $args->offsetGet('input');
        $productInput = new UpdateProductInput($inputData);

        $errors = $this->validator->validate($productInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            ->find($productInput->id);

        if (!$product || !$product instanceof Product) {
            return $this->createNotFoundException($product->getId());
        }

        if ($product->getSeller() !== $this->getUser()) {
            return $this->createAccessDeniedException($product->getId());
        }

        /** @var Product $product */
        $product = $this->fillObjectFromInput($product, $productInput);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param Argument $args
     * @return Product
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function addCategories(Argument $args): Product
    {
        return $this->addOrRemoveCategories($args, true);
    }

    /**
     * @param Argument $args
     * @return Product
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function removeCategories(Argument $args): Product
    {
        return $this->addOrRemoveCategories($args, false);
    }

    /**
     * @param Argument $args
     * @param bool $isAddAction
     * @return Product
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    private function addOrRemoveCategories(Argument $args, bool $isAddAction): Product
    {
        $inputData = $args->offsetGet('input');
        $addCategoriesInput = new AddOrRemoveCategoriesToProductInput($inputData);

        $errors = $this->validator->validate($addCategoriesInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            ->find($addCategoriesInput->id);

        if (!$product || !$product instanceof Product) {
            return $this->createNotFoundException($product->getId());
        }

        if ($product->getSeller() !== $this->getUser()) {
            return $this->createAccessDeniedException($product->getId());
        }

        $categories = $this->entityManager
            ->getRepository(Category::class)
            ->findBy([
                'id' => $addCategoriesInput->categories
            ])
        ;

        foreach ($categories as $category) {
            if ($isAddAction) {
                $product->addCategory($category);
            } else {
                $product->removeCategory($category);
            }
        }
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param Argument $args
     * @return bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete(Argument $args): bool
    {
        $inputData = $args->offsetGet('input');
        $productInput = new DeleteObjectInput($inputData);

        $errors = $this->validator->validate($productInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            ->find($productInput->id);

        if (!$product || !$product instanceof Product) {
            return $this->createNotFoundException($productInput->id);
        }

        if ($product->getSeller() !== $this->getUser()) {
            return $this->createAccessDeniedException($product->getId());
        }

        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'Product_create',
            'update' => 'Product_update',
            'addCategories' => 'Product_addCategories',
            'removeCategories' => 'Product_removeCategories',
            'delete' => 'Product_delete',
        ];
    }
}