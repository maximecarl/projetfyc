<?php
# src/GraphQL/Mutation/ProductsMutation.php
namespace App\GraphQL\Mutation;

use App\Entity\Comment;
use App\Entity\Product;
use App\GraphQL\Input\Comment\CreateCommentInput;
use App\GraphQL\Input\Comment\UpdateCommentInput;
use App\GraphQL\Input\DeleteObjectInput;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommentsMutation extends AbstractMutation
{
    /**
     * UsersMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__constructor('Comment', $entityManager, $validator, $tokenStorage);
    }

    /**
     * @param Argument $args
     * @return Comment
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function create(Argument $args): Comment
    {
        $inputData = $args->offsetGet('input');
        $commentInput = new CreateCommentInput($inputData);

        $errors = $this->validator->validate($commentInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            ->find($commentInput->product)
        ;

        if (!$product || !$product instanceof Product) {
            return $this->createNotFoundException($commentInput->product, 'Product');
        }

        /** @var Comment $comment */
        $comment = new Comment();
        $comment->setContent($commentInput->content);
        $comment->setAuthor($this->getUser());
        $comment->setProduct($product);

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $comment;
    }

    /**
     * @param Argument $args
     * @return Comment
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function update(Argument $args): Comment
    {
        $inputData = $args->offsetGet('input');
        $commentInput = new UpdateCommentInput($inputData);

        $errors = $this->validator->validate($commentInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $comment = $this->entityManager
            ->getRepository(Comment::class)
            ->find($commentInput->id)
        ;

        if (!$comment || !$comment instanceof Comment) {
            return $this->createNotFoundException($commentInput->id, 'Comment');
        }

        if ($comment->getAuthor() !== $this->getUser()) {
            return $this->createAccessDeniedException($comment->getId());
        }

        /** @var Comment $comment */
        $comment->setContent($commentInput->content);
        $this->entityManager->flush();

        return $comment;
    }

    /**
     * @param Argument $args
     * @return bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete(Argument $args): bool
    {
        $inputData = $args->offsetGet('input');
        $commentInput = new DeleteObjectInput($inputData);

        $errors = $this->validator->validate($commentInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $comment = $this->entityManager
            ->getRepository(Comment::class)
            ->find($commentInput->id);

        if (!$comment || !$comment instanceof Comment) {
            return $this->createNotFoundException($commentInput->id);
        }

        if ($comment->getAuthor() !== $this->getUser()) {
            return $this->createAccessDeniedException($comment->getId());
        }

        $this->entityManager->remove($comment);
        $this->entityManager->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'Comment_create',
            'update' => 'Comment_update',
            'delete' => 'Comment_delete',
        ];
    }
}