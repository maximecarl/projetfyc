<?php

namespace App\GraphQL\Mutation;

use App\Entity\User;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractMutation implements MutationInterface, AliasedInterface
{
    protected string $entityName;
    protected EntityManagerInterface $entityManager;
    protected ValidatorInterface $validator;
    protected TokenStorageInterface $tokenStorage;

    /**
     * @param string $entityName
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $tokenStorage
     */
    public function __constructor(
        string $entityName,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    ) {
        $this->entityName = $entityName;
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $object
     * @param $objectInput
     * @return mixed
     */
    public function fillObjectFromInput($object, $objectInput)
    {
        $objectVars = get_object_vars($objectInput);
        foreach ($objectVars as $key => $value) {
            if ($value !== null && $key !== 'id') {
                $setter = 'set' . ucfirst($key);
                if (method_exists($object, $setter)) {
                    $object->{$setter}($value);
                }
            }
        }

        return $object;
    }

    /**
     * @param array $errors
     * @throws InvalidArgumentException
     */
    public function createInvalidArgumentException(array $errors)
    {
        throw new InvalidArgumentException(
            '[%s] Invalid argument\n' . join('\n', $errors)
        );
    }

    /**
     * @param string $message
     * @throws \Exception
     */
    public function createUnauthorizedActionException(string $message)
    {
        throw new \Exception($message);
    }

    /**
     * @param int $id
     */
    public function createNotFoundException(int $id, $entityName = null)
    {
        if (!$entityName) {
            $entityName = $this->entityName;
        }
        throw new NotFoundHttpException(
            sprintf('[%s] Object with id %d not found', $entityName, $id)
        );
    }

    public function createAccessDeniedException(int $id)
    {
        throw new AccessDeniedException(
            sprintf('[%s] Object with id %d not found', $this->entityName, $id)
        );
    }

    /**
     * @return array
     */
    public static function getAliases(): array
    {
        return [];
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return null;
        }
        $user = $token->getUser();
        return $user instanceof User ? $user : null;
    }
}