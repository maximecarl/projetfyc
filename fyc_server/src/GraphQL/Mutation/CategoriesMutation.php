<?php
# src/GraphQL/Mutation/CategoriesMutation.php
namespace App\GraphQL\Mutation;

use App\Entity\Category;
use App\Entity\Product;
use App\GraphQL\Input\Category\AddOrRemoveProductsToCategoryInput;
use App\GraphQL\Input\Category\CreateCategoryInput;
use App\GraphQL\Input\DeleteObjectInput;
use App\GraphQL\Input\Category\UpdateCategoryInput;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoriesMutation extends AbstractMutation
{
    /**
     * UsersMutation constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__constructor('Category', $entityManager, $validator, $tokenStorage);
    }

    /**
     * @param Argument $args
     * @return Category
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function create(Argument $args): Category
    {
        $inputData = $args->offsetGet('input');
        $categoryInput = new CreateCategoryInput($inputData);

        $errors = $this->validator->validate($categoryInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        /** @var Category $category */
        $category = $this->fillObjectFromInput(new Category(), $categoryInput);

        $products = $this->entityManager
            ->getRepository(Product::class)
            ->findBy([
                'seller' => $this->getUser(),
                'id' => $categoryInput->products
            ])
        ;

        foreach ($products as $product) {
            $category->addProduct($product);
        }

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    /**
     * @param Argument $args
     * @return Category
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function update(Argument $args): Category
    {
        $inputData = $args->offsetGet('input');
        $categoryInput = new UpdateCategoryInput($inputData);

        $errors = $this->validator->validate($categoryInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $category = $this->entityManager
            ->getRepository(Category::class)
            ->find($categoryInput->id);

        if (!$category || !$category instanceof Category) {
            return $this->createNotFoundException($category->getId());
        }

        /** @var Category $category */
        $category = $this->fillObjectFromInput($category, $categoryInput);
        $this->entityManager->flush();

        return $category;
    }

    /**
     * @param Argument $args
     * @return Category
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function addProducts(Argument $args): Category
    {
        return $this->addOrRemoveProducts($args, true);
    }

    /**
     * @param Argument $args
     * @return Category
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function removeProducts(Argument $args): Category
    {
        return $this->addOrRemoveProducts($args, false);
    }

    /**
     * @param Argument $args
     * @param bool $isAddAction
     * @return Category
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    private function addOrRemoveProducts(Argument $args, bool $isAddAction): Category
    {
        $inputData = $args->offsetGet('input');
        $addProductsInput = new AddOrRemoveProductsToCategoryInput($inputData);

        $errors = $this->validator->validate($addProductsInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $category = $this->entityManager
            ->getRepository(Category::class)
            ->find($addProductsInput->id);

        if (!$category || !$category instanceof Category) {
            return $this->createNotFoundException($category->getId());
        }

        $products = $this->entityManager
            ->getRepository(Product::class)
            ->findBy([
                'seller' => $this->getUser(),
                'id' => $addProductsInput->products
            ])
        ;

        foreach ($products as $product) {
            if ($isAddAction) {
                $category->addProduct($product);
            } else {
                $category->removeProduct($product);
            }
        }
        $this->entityManager->flush();

        return $category;
    }

    /**
     * @param Argument $args
     * @return bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete(Argument $args): bool
    {
        $inputData = $args->offsetGet('input');
        $categoryInput = new DeleteObjectInput($inputData);

        $errors = $this->validator->validate($categoryInput);
        if ($errors->count() > 0) {
            return $this->createInvalidArgumentException($errors);
        }

        $category = $this->entityManager
            ->getRepository(Category::class)
            ->find($categoryInput->id);

        if (!$category || !$category instanceof Category) {
            return $this->createNotFoundException($categoryInput->id);
        }

        $this->entityManager->remove($category);
        $this->entityManager->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'Category_create',
            'update' => 'Category_update',
            'addProducts' => 'Category_addProducts',
            'removeProducts' => 'Category_removeProducts',
            'delete' => 'Category_delete',
        ];
    }
}