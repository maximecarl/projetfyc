<?php

namespace App\GraphQL\Resolver;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Service\QueryHelper;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;

/**
 * Class CommentsResolver
 * @package App\GraphQL\Resolver
 */
final class CommentsResolver implements QueryInterface, AliasedInterface
{
    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     *
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @param int $id
     * @return Comment|null
     */
    public function resolveFind(int $id): ?Comment
    {
        return $this->commentRepository->find($id);
    }

    /**
     * @param Argument $args
     * @return Comment[]
     */
    public function resolveFindBy(Argument $args): array
    {
        $qbHelper = new QueryHelper($this->commentRepository, 'comment');

        $qbHelper
            ->addEqualValue('id', $args['id'])
            ->addEqualValue('product', $args['product'])
            ->addEqualValue('author', $args['author'])
        ;

        return $qbHelper->execute();
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolveFind' => 'Comment_find',
            'resolveFindBy' => 'Comments_findBy',
        ];
    }
}
