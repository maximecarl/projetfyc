<?php

namespace App\GraphQL\Resolver;

use App\Entity\Discussion;
use App\Entity\Product;
use App\Repository\DiscussionRepository;
use App\Repository\ProductRepository;
use App\Service\QueryHelper;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;

/**
 * Class DiscussionsResolver
 * @package App\GraphQL\Resolver
 */
final class DiscussionsResolver implements QueryInterface, AliasedInterface
{
    /**
     * @var DiscussionRepository
     */
    private $discussionRepository;

    /**
     *
     * @param DiscussionRepository $discussionRepository
     */
    public function __construct(DiscussionRepository $discussionRepository)
    {
        $this->discussionRepository = $discussionRepository;
    }

    /**
     * @param int $id
     * @return Discussion|null
     */
    public function resolveFind(int $id): ?Discussion
    {
        return $this->discussionRepository->find($id);
    }

    /**
     * @param Argument $args
     * @return Discussion[]
     */
    public function resolveFindBy(Argument $args): array
    {
        $qbHelper = new QueryHelper($this->discussionRepository, 'discussion');

        return $qbHelper->execute();
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolveFind' => 'Discussion_find',
            'resolveFindBy' => 'Discussions_findBy',
        ];
    }
}