<?php

namespace App\GraphQL\Resolver;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\QueryHelper;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UsersResolver
 * @package App\GraphQL\Resolver
 */
final class UsersResolver extends AbstractResolver
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        UserRepository $userRepository
    ) {
        parent::__constructor($tokenStorage);
        $this->userRepository = $userRepository;
    }

    public function resolveProfile(): ?User
    {
        return $this->getUser();
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function resolveFind(int $id): ?User
    {
        return $this->userRepository->find($id);
    }

    /**
     * @param Argument $args
     * @return User[]
     */
    public function resolveFindBy(Argument $args): array
    {
        $qbHelper = new QueryHelper($this->userRepository, 'user');

        $qbHelper
            ->addEqualValue('id', $args['id'])
            ->addLikeString(['username'], $args['username'])
            ->addLikeString(['email'], $args['email'])
        ;

        return $qbHelper->execute();
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolveProfile' => 'User_profile',
            'resolveFind' => 'User_find',
            'resolveFindBy' => 'Users_findBy',
        ];
    }
}
