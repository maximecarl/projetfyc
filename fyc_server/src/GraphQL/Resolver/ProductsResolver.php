<?php

namespace App\GraphQL\Resolver;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\QueryHelper;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ProductsResolver
 * @package App\GraphQL\Resolver
 */
final class ProductsResolver extends AbstractResolver
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductsResolver constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param ProductRepository $productRepository
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        ProductRepository $productRepository
    ) {
        parent::__constructor($tokenStorage);
        $this->productRepository = $productRepository;
    }

    /**
     * @param int $id
     * @return Product|null
     */
    public function resolveFind(int $id): ?Product
    {
        return $this->productRepository->find($id);
    }

    /**
     * @param Argument $args
     * @return Product[]
     */
    public function resolveFindBy(Argument $args): array
    {
        $qbHelper = new QueryHelper($this->productRepository, 'product');

        $qbHelper
            ->addEqualValue('id', $args['id'])
            ->addLikeString(['label'], $args['label'])
            ->addMinValue('price', $args['minPrice'])
            ->addMaxValue('price', $args['maxPrice'])
            ->addEqualValue('seller', $args['seller'])
        ;

        $currentUser = $this->getUser();
        if ($currentUser) {
            if ($args->offsetExists('hideMine') && $args['hideMine']) {
                $qbHelper
                    ->addNotEqualValue('seller', $currentUser);
            }
        }

        if ($args->offsetExists('availability')) {
            if ($args['availability']) {
                $qbHelper
                    ->addMinValue('quantity', 1)
                ;
            } else {
                $qbHelper
                    ->addMaxValue('quantity', 0)
                ;
            }
        }

        return $qbHelper->execute();
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolveFind' => 'Product_find',
            'resolveFindBy' => 'Products_findBy',
        ];
    }
}
