<?php

namespace App\GraphQL\Resolver;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Service\QueryHelper;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;

/**
 * Class CategoriesResolver
 * @package App\GraphQL\Resolver
 */
final class CategoriesResolver implements QueryInterface, AliasedInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param int $id
     * @return Category|null
     */
    public function resolveFind(int $id): ?Category
    {
        return $this->categoryRepository->find($id);
    }

    /**
     * @param Argument $args
     * @return Category[]
     */
    public function resolveFindBy(Argument $args): array
    {
        $qbHelper = new QueryHelper($this->categoryRepository, 'category');

        $qbHelper
            ->addEqualValue('id', $args['id'])
            ->addLikeString(['label'], $args['label'])
        ;

        return $qbHelper->execute();
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolveFind' => 'Category_find',
            'resolveFindBy' => 'Categories_findBy',
        ];
    }
}
