<?php

namespace App\GraphQL\Resolver;

use App\Entity\User;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AbstractResolver implements QueryInterface, AliasedInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __constructor(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return null;
        }
        $user = $token->getUser();
        return $user instanceof User ? $user : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [];
    }
}