<?php

namespace App\GraphQL\Input;

class AutoFillInput
{
    /**
     * AutoFillInput constructor.
     * @param array $inputData
     */
    public function __construct(array $inputData = [])
    {
        foreach ($inputData as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
}