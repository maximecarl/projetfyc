<?php

namespace App\GraphQL\Input\Discussion;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateMessageDiscussionInput
 * @package App\GraphQL\Input
 */
class SendMessageDiscussionInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $discussion;

    /**
     * @GQL\Field(type="String!")
     */
    public $content;
}