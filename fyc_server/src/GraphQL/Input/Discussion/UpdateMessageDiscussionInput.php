<?php

namespace App\GraphQL\Input\Discussion;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class UpdateMessageDiscussionInput
 * @package App\GraphQL\Input
 */
class UpdateMessageDiscussionInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="String!")
     */
    public $content;
}