<?php

namespace App\GraphQL\Input\Discussion;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateDiscussionInput
 * @package App\GraphQL\Input
 */
class CreateDiscussionInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $product;
}