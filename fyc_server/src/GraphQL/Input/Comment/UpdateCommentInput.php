<?php

namespace App\GraphQL\Input\Comment;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class UpdateProductInput
 * @package App\GraphQL\Input
 */
class UpdateCommentInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="String!")
     */
    public $content;
}