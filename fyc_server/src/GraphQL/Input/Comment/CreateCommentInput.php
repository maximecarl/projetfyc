<?php

namespace App\GraphQL\Input\Comment;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateUserInput
 * @package App\GraphQL\Input
 */
class CreateCommentInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="String!")
     */
    public $content;

    /**
     * @GQL\Field(type="Int")
     */
    public $product;
}