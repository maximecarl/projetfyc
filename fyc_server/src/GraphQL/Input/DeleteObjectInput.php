<?php

namespace App\GraphQL\Input;

use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class DeleteObjectInput
 * @package App\GraphQL\Input
 */
class DeleteObjectInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;
}