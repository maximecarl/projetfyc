<?php

namespace App\GraphQL\Input\User;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateUserInput
 * @package App\GraphQL\Input
 */
class CreateUserInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="String!")
     */
    public $email;

    /**
     * @GQL\Field(type="String!")
     */
    public $username;

    /**
     * @GQL\Field(type="String!")
     */
    public $password;
}