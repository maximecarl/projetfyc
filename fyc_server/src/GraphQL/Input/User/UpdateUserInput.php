<?php

namespace App\GraphQL\Input\User;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class UpdateUserInput
 * @package App\GraphQL\Input
 */
class UpdateUserInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int")
     */
    public $id;

    /**
     * @GQL\Field(type="String")
     */
    public $username;

    /**
     * @GQL\Field(type="String")
     */
    public $email;
}