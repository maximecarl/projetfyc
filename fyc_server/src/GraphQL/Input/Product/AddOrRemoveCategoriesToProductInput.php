<?php

namespace App\GraphQL\Input\Category;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class AddOrRemoveCategoriesToProductInput
 * @package App\GraphQL\Input
 */
class AddOrRemoveCategoriesToProductInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="[Int]!")
     */
    public $categories;
}