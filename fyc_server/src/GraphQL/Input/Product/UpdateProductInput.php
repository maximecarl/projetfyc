<?php

namespace App\GraphQL\Input\Product;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class UpdateProductInput
 * @package App\GraphQL\Input
 */
class UpdateProductInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="String")
     */
    public $label;

    /**
     * @GQL\Field(type="Float")
     */
    public $price;

    /**
     * @GQL\Field(type="Int")
     */
    public $quantity;
}