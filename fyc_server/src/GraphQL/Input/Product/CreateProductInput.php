<?php

namespace App\GraphQL\Input\Product;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateUserInput
 * @package App\GraphQL\Input
 */
class CreateProductInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="String!")
     */
    public $label;

    /**
     * @GQL\Field(type="Float!")
     */
    public $price;

    /**
     * @GQL\Field(type="Int!")
     */
    public $quantity;

    /**
     * @GQL\Field(type="[Int]")
     */
    public $categories;
}