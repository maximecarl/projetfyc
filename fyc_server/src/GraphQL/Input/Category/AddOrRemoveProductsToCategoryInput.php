<?php

namespace App\GraphQL\Input\Category;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class AddOrRemoveProductsToCategoryInput
 * @package App\GraphQL\Input
 */
class AddOrRemoveProductsToCategoryInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="[Int]!")
     */
    public $products;
}