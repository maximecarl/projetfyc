<?php

namespace App\GraphQL\Input\Category;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class UpdateCategoryInput
 * @package App\GraphQL\Input
 */
class UpdateCategoryInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="Int!")
     */
    public $id;

    /**
     * @GQL\Field(type="String")
     */
    public $label;
}