<?php

namespace App\GraphQL\Input\Category;

use App\GraphQL\Input\AutoFillInput;
use Overblog\GraphQLBundle\Annotation as GQL;

/**
 * @GQL\Input
 *
 * Class CreateCategoryInput
 * @package App\GraphQL\Input
 */
class CreateCategoryInput extends AutoFillInput
{
    /**
     * @GQL\Field(type="String!")
     */
    public $label;

    /**
     * @GQL\Field(type="[Int]")
     */
    public $products = [];
}