<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Yaml\Yaml;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;
    private $data;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
        $this->data = Yaml::parseFile('src/DataFixtures/data.yaml');
    }

    public function load(ObjectManager $manager)
    {
        /** USER */
        $userData = $this->data['users'];
        $mainUser = null;
        foreach ($userData as $userDatum) {
            $user = new User();
            $user->setEmail($userDatum['email']);
            $user->setUsername($userDatum['username']);
            $user->setPassword(
                $this->passwordHasher->hashPassword($user, $userDatum['password'])
            );

            $manager->persist($user);
            if (!$mainUser) {
                $mainUser = $user;
            }
        }

        /** CATEGORY */
        $categoriesData = $this->data['categories'];
        $categories = [];
        foreach ($categoriesData as $categoryDatum) {
            $category = new Category();
            $category->setLabel($categoryDatum);

            $manager->persist($category);
            array_push($categories, $category);
        }

        /** PRODUCT */
        $productsData = $this->data['products'];
        foreach ($productsData as $productsDatum) {
            $product = new Product();
            $product->setLabel($productsDatum['label']);
            $product->setPrice($productsDatum['price']);
            $product->setQuantity($productsDatum['quantity']);
            $product->setSeller($mainUser);

            foreach ($productsDatum['categories'] as $categoryLabel) {
                $matchingCategories = array_filter(
                    $categories,
                    function(Category $category) use ($categoryLabel) {
                        return $category->getLabel() == $categoryLabel;
                    }
                );

                if (count($matchingCategories) > 0) {
                    $category = array_pop($matchingCategories);
                    if ($category instanceof Category) {
                        $product->addCategory($category);
                    }
                }
            }

            $manager->persist($product);
        }

        $manager->flush();
    }
}