# Projet FYC

## Installation
Pour lancer l'application, veuillez télécharger ce repository, puis lancer ces lignes de codes :

- Lancer les conteneurs docker
```
docker-compose up -d --build
```

- Installer les paquets
```
docker-compose exec apache-php composer install
```

- Lancer et mettre à jour le schéma de base de données
```
docker-compose exec apache-php php bin/console doctrine:database:create
docker-compose exec apache-php php bin/console doctrine:schema:update --force
docker-compose exec apache-php php bin/console doctrine:fixtures:load
```

- Lancer le front-end
```
cd fyc_client
npm install
npm start
```

## Connexion
URL BACK : http://localhost:8000/
- Login : http://localhost:8000/api/login
- API : http://localhost:8000/api/

URL FRONT : http://localhost:3000/

Se connecter à l'application avec les identifiants suivants :
- username : maxime.carl
- mot de passe : pwd123

## TP 
- Introduction
Branche feature/start
- Créer le type Product
- Créer la Query Products
- Créer la Mutation CreateProduct
Branche feature/autonomie
- Créer le type Comment
- Créer les Queries de Comment
- Créer les Mutations de création, modification et de suppression de Comment